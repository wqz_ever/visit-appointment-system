import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
Vue.use(VueRouter)
const router = new VueRouter({
  routes: [
    {
      path: '/login', component: Login
    },
    {
      path: '/', redirect: '/login'
    },
    {
      path: '/home', component: Home
    }
  ]
})

router.beforeEach((to, from, next) => {
  // to 将要访问的地址
  // from 从哪个路径跳转而来
  // next 函数 放行  next()直接放行  next(./login) 跳转地址
  if (to.path === '/login') return next()
  const tokenStr = window.sessionStorage.getItem('token')
  if (tokenStr === null) return next('/login')
  next()
})

export default router
