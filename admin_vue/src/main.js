import Vue from 'vue'
import './assets/css/main.css'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import './plugins/element.js'
import { Loading } from 'element-ui'

let loading
function loadingStart () {
  loading = Loading.service({
    lock: true,
    text: '努力加载中....',
    background: 'rgba(0,0,0,.7)'
  })
}
function endLoading () {
  loading.close()
}
axios.interceptors.request.use(config => {
  config.headers.Authorization = window.sessionStorage.getItem('token')
  config.headers.user = 'admin'
  // NProgress.start()
  loadingStart()
  return config
})
axios.interceptors.response.use(
  response => {
    endLoading()
    return response
  },
  error => {
    endLoading()
    const { status } = error.response
    if (status === 401) {
      window.sessionStorage.removeItem('token')
      router.push('/login')
      alert('Token已过期，请重新登陆')
    }
    return Promise.reject(error)
  })
Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.prototype.$bus = new Vue()

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
