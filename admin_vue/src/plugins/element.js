import Vue from 'vue'
import {
  Button, Form, Input, FormItem, Icon, Container, Aside, Header, Main, Menu,
  MenuItem, Submenu, MenuItemGroup, Tooltip, Card, Tag, Avatar, Breadcrumb,
  BreadcrumbItem, Table, TableColumn, Pagination, Alert, Cascader, Select,
  Option, Row, DatePicker, Message, Dialog, MessageBox, Radio, TimePicker, RadioGroup, Switch, Tabs, TabPane, Notification
} from 'element-ui'

Vue.use(Button)
Vue.use(Form)
Vue.use(Input)
Vue.use(FormItem)
Vue.use(Icon)
Vue.use(Container)
Vue.use(Aside)
Vue.use(Header)
Vue.use(Main)
Vue.use(Menu)
Vue.use(MenuItem)
Vue.use(Submenu)
Vue.use(MenuItemGroup)
Vue.use(Tooltip)
Vue.use(Card)
Vue.use(Tag)
Vue.use(Avatar)
Vue.use(Breadcrumb)
Vue.use(BreadcrumbItem)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Pagination)
Vue.use(Alert)
Vue.use(Cascader)
Vue.use(Select)
Vue.use(Option)
Vue.use(Row)
Vue.use(DatePicker)
Vue.use(Dialog)
Vue.use(Radio)
Vue.use(TimePicker)
Vue.use(RadioGroup)
Vue.use(Switch)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.prototype.$confirm = MessageBox
Vue.prototype.$message = Message
Vue.prototype.$notify = Notification
