import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../Login.vue'
import Home from '../Home.vue'
import Index from '../components/Index/Index.vue'
import Offline from '../components/Offline/Offline.vue'
import Order from '../components/order/Order.vue'
import SetUp from '../components/setUP/SetUp.vue'
import Myorder from '../components/order/Myorder.vue'
import MyofflineOrder from '../components/Offline/Myofflineorder.vue'
import Instructions from '../components/Index/Instructions.vue'
import Nutrients from '../components/Index/Nutrients.vue'
import BMI from '../components/Index/BMI.vue'
import Medical from '../components/Index/Medical.vue'
Vue.use(VueRouter)
const router = new VueRouter({
  routes: [
    { path: '/login', component: Login },
    { path: '/', redirect: '/login' },
    { path: '/home', redirect: '/index ' },
    {
      path: '/home',
      component: Home,
      children: [
        { path: '/index', component: Index },
        { path: '/offline', component: Offline },
        { path: '/order', component: Order },
        { path: '/setUp', component: SetUp },
        { path: '/myorder', component: Myorder },
        { path: '/myofflineorder', component: MyofflineOrder },
        { path: '/instructions', component: Instructions },
        { path: '/nutrients', component: Nutrients },
        { path: '/bmi', component: BMI },
        { path: '/medical', component: Medical }

      ]

    }
  ]
})
router.beforeEach((to, from, next) => {
  // to 将要访问的地址
  // from 从哪个路径跳转而来
  // next 函数 放行  next()直接放行  next(./login) 跳转地址
  if (to.path === '/login') return next()
  const tokenStr = window.sessionStorage.getItem('token')
  if (tokenStr === null) return next('/login')
  next()
})

export default router
