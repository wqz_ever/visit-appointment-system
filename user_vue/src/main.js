import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './assets/css/main.css'
import axios from 'axios'
import Vant, { Toast, Dialog } from 'vant'
import 'vant/lib/index.css'
import './plugins/element.js'

// 全局注册
Vue.use(Vant)
Vue.use(Toast)
axios.interceptors.request.use(config => {
  config.headers.Authorization = window.sessionStorage.getItem('token')
  // NProgress.start()
  Toast.loading({
    message: '加载中...',
    forbidClick: true
  })
  return config
})
axios.interceptors.response.use(
  response => {
    Toast.clear()
    return response
  },
  error => {
    Toast.clear()
    const { status } = error.response
    if (status === 401) {
      window.sessionStorage.removeItem('token')
      router.push('/login')
      alert('Token已过期，请重新登陆')
    }
    return Promise.reject(error)
  })
Vue.config.productionTip = false
Vue.prototype.$http = axios
Vue.prototype.$Toast = Toast
Vue.prototype.$Dialog = Dialog
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
