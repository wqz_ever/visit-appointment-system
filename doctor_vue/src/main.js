import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import './assets/css/main.css'
import './plugins/wyz-echarts/wyz-echarts.js'
import scroll from 'vue-seamless-scroll'
import axios from 'axios'
import VCharts from 'v-charts-v2'
import { Loading } from 'element-ui'

let loading
function loadingStart () {
  loading = Loading.service({
    lock: true,
    text: '努力加载中....',
    background: 'rgba(0,0,0,.7)'
  })
}
function endLoading () {
  loading.close()
}
axios.interceptors.request.use(config => {
  config.headers.Authorization = window.sessionStorage.getItem('token')
  // NProgress.start()
  loadingStart()
  return config
})
axios.interceptors.response.use(
  response => {
    endLoading()
    return response
  },
  error => {
    endLoading()
    const { status } = error.response
    if (status === 401) {
      window.sessionStorage.removeItem('token')
      router.push('/login')
      alert('Token已过期，请重新登陆')
    }
    return Promise.reject(error)
  })
Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.use(scroll)
Vue.use(VCharts)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
