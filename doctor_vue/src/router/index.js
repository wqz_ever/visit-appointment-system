import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../Login.vue'
import Home from '../Home.vue'
import Welcome from '../views/Welcome.vue'
import Person from '../components/Person/Peson.vue'
import InformationList from '../components/Informasion/InformasionList.vue'
import InformasionView from '../components/Informasion/InformasionView.vue'
// import HealthSelect from '../components/health/HealthSelect.vue'
// import HealthView from '../components/health/HealthView.vue'
import OnlineList from '../components/online/OnlineList.vue'
import Announcement from '../components/online/Announcement.vue'
import ConsultationList from '../components/Consultation/ConsultationList.vue'
import ConsultationOrder from '../components/Consultation/ConsultationOrder.vue'
import DoctorList from '../components/admin/DoctorList.vue'
import UserList from '../components/admin/userList.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/login',
      component: Login,
      children: [
        // { path: '/register', component: Register }
      ]
    },
    { path: '/', redirect: '/login' },
    {
      path: '/home',
      component: Home,
      redirect: '/welcome',
      children:
        [
          { path: '/welcome', component: Welcome },
          { path: '/person', component: Person },
          { path: '/information_List', component: InformationList },
          { path: '/information_View', component: InformasionView },
          { path: '/online_List', component: OnlineList },
          { path: '/announcement', component: Announcement },
          { path: '/consultation_List', component: ConsultationList },
          { path: '/consultation_Order', component: ConsultationOrder },
          { path: '/doctorList', component: DoctorList },
          { path: '/userLists', component: UserList }
        ]
    }
  ]
})
router.beforeEach((to, from, next) => {
  // to 将要访问的地址
  // from 从哪个路径跳转而来
  // next 函数 放行  next()直接放行  next(./login) 跳转地址
  if (to.path === '/login') return next()
  const tokenStr = window.sessionStorage.getItem('token')
  if (tokenStr === null) return next('/login')
  next()
})

export default router
