const express = require('express')
const router = express.Router()
const Msgcard = require('../../models/Msgcard')
const Offline = require('../../models/Offlintreatment')
const passport = require('passport')
const url = require("url")
router.post('/addofflineorder/:patientID',(req, res) => {
    const info = {}
    if(req.body.message) info.message = req.body.message
    if(req.body.doctorID) info.doctorID = req.body.doctorID
    if(req.body.msgCardID) info.msgCardID = req.body.msgCardID
    info.patientID = req.params.patientID
    new  Offline(info).save().then( success => {
        return res.json({status:200,msg:'success'})
    })
})
router.get('/myorder/:patientID',(req,res) => {
    let myorderList = []
    let infolist = {}
    let newofflist = []
    Offline.find({patientID:req.params.patientID}).then( offlist => {
        if(offlist.length == 0) return res.json({status:404,error:'error'})
        newofflist = offlist
        newofflist.forEach(item => {
            Msgcard.findOne({_id:item.msgCardID}).then(info => {
                infolist.name = info.name
                infolist.address = info.address
                infolist.phone = info.phone
                infolist.message = item.message
                infolist.status = item.status
                infolist.orderDate = item.orderDate
                infolist.offlineDate = item.offlineDate
                myorderList = myorderList.concat(infolist)
                infolist = {}
                if(myorderList.length === newofflist.length){
                    return res.json({status:200,orderList:myorderList})
                }
            })
            // console.log(myorderList)
        })
    })
    
})
router.get('/offlineList/:doctorID',passport.authenticate('jwt', { session: false }), (req, res) => {
    let infoList = []
    let newInfo = {}
    let newinfoList = []
    Offline.find({doctorID:req.params.doctorID}).then( list => {
        if(list.length === 0) return res.json({status:404,erro:'error'})
        newinfoList = list
        newinfoList.forEach(item => {
            Msgcard.findOne({_id:item.msgCardID}).then( info => {
                newInfo.id = item._id
                newInfo.name = info.name
                newInfo.age = info.age
                newInfo.sex = info.sex
                newInfo.phone = info.phone
                newInfo.address = info.address
                newInfo.message = item.message
                newInfo.orderDate = item.orderDate
                newInfo.status = item.status
                newInfo.offlineDate = item.offlineDate
                // console.log(newInfo)
                infoList=infoList.concat(newInfo)
                newInfo = {}
                if(infoList.length === newinfoList.length){
                    return res.json({status:200,offlineList:infoList})
                }
            })
        });
    })
})
router.get('/info/:id',passport.authenticate('jwt', { session: false }), (req, res) => {
    Offline.findOne({_id:req.params.id}).then( info => {
        return res.json({status:200,info:info})
    })
})
router.post('/edit/:id',passport.authenticate('jwt', { session: false }), (req, res) => {
    Offline.findOneAndUpdate({_id:req.params.id},{$set:{status:req.body.status,offlineDate:req.body.offlineDate}},{new:true}).then( success => {
        return res.json({status:200,msg:'success'})
    })
})
router.get('/alloffline',(req,res) => {
    const queryInfo = url.parse(req.url, true).query
    const size = parseInt(queryInfo.pagesize)
    const page = parseInt(queryInfo.pagenum)
    const query = queryInfo.query
    const start = (page - 1) * size   
    if (query) {
        Offline.find().where('name', query)
            .then(order => {
                const newOrderList = { orderList: order, total: order.length,status:200 }
                res.json(newOrderList)
            })
    } else {
        Offline.find()
            .then(count => {
                const total = count.length
                Offline.find().limit(size).skip(start)
                    .then(order => {
                        if (!order) {
                            return res.status(400).json("获取列表数据失败")
                        }
                        const newOrderList = { orderList: order, total: total,status:200}
                        res.json(newOrderList)
                    })
                    .catch(err => res.status(400).json(err))
            })
    }
})
module.exports = router