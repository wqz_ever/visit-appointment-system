const express = require('express')
const router = express.Router()
const Profile = require('../../models/Profile')
const passport = require('passport')
const url = require("url")
router.post('/add/:doctorID', passport.authenticate('jwt', { session: false }), (req, res) => {
    const profileFields = {}
    if (req.body.name) profileFields.name = req.body.name
    if (req.body.age) profileFields.age = req.body.age
    if (req.body.sex) profileFields.sex = req.body.sex
    if (req.body.phone) profileFields.phone = req.body.phone
    if (req.body.symptom) {
        if (req.body.tag === '1') { profileFields.symptom = req.body.symptom } else {
            profileFields.symptom = (req.body.symptom).split('/')
        }
    }
    if (req.body.isCure) profileFields.isCure = req.body.isCure
    if(req.params.doctorID) profileFields.doctorID = req.params.doctorID
    new Profile(profileFields).save().then(profile => {
        res.json(profile)
    })
})

router.get('/patientList/:doctorID', passport.authenticate('jwt', { session: false }), (req, res) => {
    const queryInfo = url.parse(req.url, true).query
    const size = parseInt(queryInfo.pagesize)
    const page = parseInt(queryInfo.pagenum)
    const query = queryInfo.query
    const start = (page - 1) * size   
    if(query){
        Profile.find({doctorID:req.params.doctorID}).where('name',query).then(profile => {
            if(!profile){
                return res.status(201).json('未查到此人')
            }
            const newProfile = {information:profile,total:profile.length}
            res.status(200).json(newProfile)
        })
    }else{
    Profile.find({doctorID:req.params.doctorID})
        .then(count => {
            const total = count.length
            Profile.find({doctorID:req.params.doctorID}).limit(size).skip(start)
                .then(profile => {
                    if (!profile) {
                        return res.status(201).json("获取列表数据失败")
                    }
                    const newProfile = { information: profile, total: total }
                    res.send(newProfile)
                })
                .catch(err => res.status(400).json(err))
        })
    }
})
router.get('/patientinfo/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    Profile.findOne({ _id: req.params.id })
        .then(profile => {
            if (!profile) {
                return res.status(400).json("获取数据失败")
            }
            res.json(profile)
        })
        .catch(err => res.status(400).json(err))
})
router.post('/edit/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    const profileFields = {}
    if (req.body.name) profileFields.name = req.body.name
    if (req.body.age) profileFields.age = req.body.age
    if (req.body.sex) profileFields.sex = req.body.sex
    if (req.body.phone) profileFields.phone = req.body.phone
    if (req.body.symptom) profileFields.symptom = req.body.symptom.split('/')
    if (req.body.isCure) profileFields.isCure = req.body.isCure
    Profile.findOneAndUpdate({ _id: req.params.id }, { $set: profileFields }, { new: true }).then(profile => res.json(profile))
})
router.delete('/delete/:id',passport.authenticate('jwt', { session: false }), (req, res) => {
    // var _id =  new ObjectId(req.params.id)
    Profile.deleteOne({_id:req.params.id})
           .then(profile => {
               return res.status(200)
           })
})
module.exports = router