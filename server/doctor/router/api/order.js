const express = require('express')
const router = express.Router()
const Order = require('../../models/Order')
// const JwtStrategy = require('passport-jwt').Strategy,
const passport = require('passport')
var url = require("url")

router.get('/orderList/:doctorID', passport.authenticate('jwt', { session: false }), (req, res) => {
    const queryInfo = url.parse(req.url, true).query
    const size = parseInt(queryInfo.pagesize)
    const page = parseInt(queryInfo.pagenum)
    const query = queryInfo.query
    const start = (page - 1) * size
    if (query) {
        Order.find({ doctorID: req.params.doctorID }).where('name', query)
            .then(order => {
                const newOrderList = { orderList: order, total: order.length }
                res.status(200).json(newOrderList)
            })
    } else {
        Order.find({ doctorID: req.params.doctorID })
            .then(count => {
                const total = count.length
                Order.find({ doctorID: req.params.doctorID }).limit(size).skip(start)
                    .then(order => {
                        if (!order) {
                            return res.status(400).json("获取列表数据失败")
                        }
                        const newOrderList = { orderList: order, total: total }
                        res.status(200).json(newOrderList)
                    })
                    .catch(err => res.status(400).json(err))
            })
    }

})
router.post('/add/:doctorID', (req, res) => {
    const newOrderList = {}
    if (req.body.name) newOrderList.name = req.body.name
    if (req.body.age) newOrderList.age = req.body.age
    if (req.body.sex) newOrderList.sex = req.body.sex
    if (req.body.symptom) newOrderList.symptom = req.body.symptom.split('/')
    if (req.body.phone) newOrderList.phone = req.body.phone
    if (req.body.orderDate) newOrderList.orderDate = req.body.orderDate
    if (req.params.doctorID) newOrderList.doctorID = req.params.doctorID
    new Order(newOrderList).save().then(order => {
        res.status(200).json({ status: 200, info: order })
    })
})
router.post('/edit/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    const newOrderList = {}
    if (req.body.orderDate) newOrderList.orderDate = req.body.orderDate
    if(req.body.address) newOrderList.address = req.body.address
    Order.findOneAndUpdate({ _id: req.params.id }, { $set: newOrderList }, { new: true })
        .then(order => res.status(200).json(order))
        .catch(err => res.status(201).json(err))

})
router.get('/orderinfo/:id',(req, res) => {
    Order.findOne({ _id: req.params.id })
        .then(order => {
            if (!order) {
                return res.status(400).json('获取数据失败')
            }
            return res.json({status:200,info:order})
        })
        .catch(err => { return err })
})
router.delete('/delete/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    Order.deleteOne({ _id: req.params.id })
        .then(order => {
           return res.json({status:200,msg:'成功'})
        })
        .catch(err => { res.status(400).json(err) })
})
router.get('/allorders',(req, res) => {
    const queryInfo = url.parse(req.url, true).query
    const size = parseInt(queryInfo.pagesize)
    const page = parseInt(queryInfo.pagenum)
    const query = queryInfo.query
    const start = (page - 1) * size
    if (query) {
        Order.find().where('name', query)
            .then(order => {
                const newOrderList = { orderList: order, total: order.length,status:200 }
                res.json(newOrderList)
            })
    } else {
        Order.find()
            .then(count => {
                const total = count.length
                Order.find().limit(size).skip(start)
                    .then(order => {
                        if (!order) {
                            return res.status(400).json("获取列表数据失败")
                        }
                        const newOrderList = { orderList: order, total: total,status:200}
                        res.json(newOrderList)
                    })
                    .catch(err => res.status(400).json(err))
            })
    }

})
module.exports = router