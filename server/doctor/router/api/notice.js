const express = require('express')
const router = express.Router()
const Notice = require('../../models/Notice')
const passport = require('passport')
var url = require("url")
router.post('/message/:orderID',passport.authenticate('jwt', { session: false }), (req, res) => {
    const message ={}
    if(req.body.noticeMessage) message.noticeMessage = req.body.noticeMessage
    message.orderID = req.paarams.orderID
    new Notice(message).save().then(notice => {
        res.status(200).json(notice)
    })
    .catch(err => {
        res.status(400).json(err)
    })
})
router.get('/notice/:orderID',(req,res) => {
    Notice.find({orderID:req.params.orderID}).then(notice => {
        if(!notice){return res.json({status:201,msg:'暂时未收到通知'})}else{
            return res.json({status:200,notice:notice})
        }
    })
})
module.exports = router