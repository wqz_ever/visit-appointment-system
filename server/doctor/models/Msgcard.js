const mongoose = require('mongoose')
const Scheam = mongoose.Schema
const Patient = require('../models/Patient')
const Msgcard = new Scheam({
    name:{
        type:String,
        required:true
    },
    age:{
        type:Number,
        required:true
    },
    sex:{
        type:String,
        required:true
    },
    address:{
        type:String,
        required:true
    },
    phone:{
        type:Number,
        required:true
    },
    CHID:{
        type:Number,
        required:true
    },
    patientID:{
        type:mongoose.Schema.Types.ObjectId,
        ref:Patient
    }
})
module.exports = Notice = mongoose.model('msgcard',Msgcard)