const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const Doctor = require('../models/Doctor')
const ProfileSchema = new Schema({
    name: {
        type: String
    },
    age: {
        type: String
    },
    sex: {
        type: String
    },
    symptom: {
        type: []
    },
    phone: {
        type: String
    },
    isCure: {
        type: String,
        default: '0'
    },
    date: {
        type: Date,
        default: Date.now()
    },
    charDate: {
        type:Object,
        default:{
            temperatureChartData: {
                columns: ['日期', '体温'],
                rows: [
                    { '日期': '星期一', '体温': 36.6, },
                    { '日期': '星期二', '体温': 36.8, },
                    { '日期': '星期三', '体温': 36.6, },
                    { '日期': '星期四', '体温': 38, },
                    { '日期': '星期五', '体温': 37.4, },
                    { '日期': '星期六', '体温': 37, },
                    { '日期': '星期日', '体温': 37, }
                ]
            },
            bloodPressureCharDate: {
                columns: ['日期', '大压', '小压'],
                rows: [
                    { '日期': '星期一', '大压': 120, '小压': 80 },
                    { '日期': '星期二', '大压': 127, '小压': 88 },
                    { '日期': '星期三', '大压': 130, '小压': 70 },
                    { '日期': '星期四', '大压': 125, '小压': 80 },
                    { '日期': '星期五', '大压': 120, '小压': 82 },
                    { '日期': '星期六', '大压': 123, '小压': 83 },
                    { '日期': '星期日', '大压': 139, '小压': 84 }
                ]
            },
            bloodSurgerCharDate:{
                columns: ['日期', '空腹血糖值(mmol/L)','餐后2小时血糖值(mmol/L)'],
                rows: [
                    { '日期': '星期一', '空腹血糖值(mmol/L)': 5.1,'餐后2小时血糖值(mmol/L)':7.8},
                    { '日期': '星期二', '空腹血糖值(mmol/L)': 3.1,'餐后2小时血糖值(mmol/L)':6.1},
                    { '日期': '星期三', '空腹血糖值(mmol/L)': 5.1,'餐后2小时血糖值(mmol/L)':6.1},
                    { '日期': '星期四', '空腹血糖值(mmol/L)': 3.1,'餐后2小时血糖值(mmol/L)':5.3},
                    { '日期': '星期五', '空腹血糖值(mmol/L)': 4.5,'餐后2小时血糖值(mmol/L)':6.1},
                    { '日期': '星期六', '空腹血糖值(mmol/L)': 3.1,'餐后2小时血糖值(mmol/L)':6.1},
                    { '日期': '星期日', '空腹血糖值(mmol/L)': 1.1,'餐后2小时血糖值(mmol/L)':5.1}
                ]
            }
        },
    },
    doctorID:{
        type:mongoose.Schema.Types.ObjectId,
        ref:Doctor
    }
})
module.exports = Profile = mongoose.model("Profile", ProfileSchema)