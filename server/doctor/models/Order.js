const mongoose = require('mongoose')
const Schema  = mongoose.Schema;
const Doctor = require('../models/Doctor')
const OrderSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    age:{
        type:String,
        required:true
    },
    sex:{
        type:String,
        required:true
    },
    symptom:{
        type:[],
        required:true
    },
    phone:{
        type:String,
        required:true
    },
    date:{
        type:Date,
        default:Date.now()
    },
    orderDate:{
        type:Date,
        default:''
    },
    tag:{
        type:String,
        default:'1'
    },
    doctorID:{
        type:mongoose.Schema.Types.ObjectId,
        ref:Doctor
    },
    address:{
        type:String,
        default:''
    }
})
module.exports =  Order  = mongoose.model("Order",OrderSchema)