const mongoose = require('mongoose')
const Schema  = mongoose.Schema;
const Doctor = require('../models/Doctor')
const UserSchema = new Schema({
    username:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    date:{
        type:Date,
        default:Date.now()
    },
    jobNumber:{
        type:String,
    },
    menu:{
        type:Object,
        default:[
            {
              id: '1',
              name: '个人信息',
              icon: 'el-icon-user-solid color1',
              children: [
                {
                  id: 1 - 1,
                  name: '个人信息展示',
                  icon: 'el-icon-user color4',
                  path: 'person'
                }
              ]
            },
            {
              id: '2',
              name: '病人信息管理',
              icon: 'el-icon-s-custom color2',
              children: [
                {
                  id: 2 - 1,
                  name: '病人信息列表',
                  icon: 'el-icon-s-order color4',
                  path: 'information_List'
                },
                {
                  id: 2 - 2,
                  name: '病人数据视图',
                  icon: 'el-icon-s-data color4',
                  path: 'information_View'
                }
              ]
            },
            {
              id: '4',
              name: '消息管理',
              icon: 'el-icon-chat-line-round color1',
              children: [
                {
                  id: 4 - 2,
                  name: '发布通告',
                  icon: 'el-icon-edit-outline color4',
                  path: 'announcement'
                }
              ]
            },
            {
              id: '5',
              name: '问诊管理',
              icon: 'el-icon-message-solid color3',
              children: [
                {
                  id: 5 - 1,
                  name: '预约上门问诊列表',
                  icon: 'el-icon-monitor color4',
                  path: 'consultation_List'
                },
                {
                  id: 5 - 2,
                  name: '预约问诊列表',
                  icon: 'el-icon-alarm-clock color4 ',
                  path: 'consultation_Order'
                }
              ]
            }
          ]
    },
    doctorID:{
      type:mongoose.Schema.Types.ObjectId,
      res:Doctor
    }
})
module.exports =  User  = mongoose.model("users",UserSchema)