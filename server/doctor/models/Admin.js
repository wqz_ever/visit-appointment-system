const mongoose = require('mongoose')
const Schema  = mongoose.Schema;
const adminSchema = new Schema({
    username:{
        type:String,
        required:true,
        default:'admin_wqz'
    },
    password:{
        type:String,
        required:true,
        default:'123456'
    },
    adminNumber:{
        type:String,
        default:'wqz666'
    },
    menu:{
        type:Object,
        default:[
            {
              id: '1',
              name: '医生信息',
              icon: 'el-icon-user-solid color1',
              children: [
                {
                  id: 1 - 1,
                  name: '医生信息列表',
                  icon: 'el-icon-user color4',
                  path: 'doctorList'
                }
              ]
            },
            {
                id: '2',
                name: '用户信息',
                icon: 'el-icon-user-solid color1',
                children: [
                  {
                    id: 2 - 1,
                    name: '用户信息列表',
                    icon: 'el-icon-user color4',
                    path: 'userLists'
                  }
                ]
              },
        ]
    }
  
})
module.exports = Admin = mongoose.model('admin',adminSchema)