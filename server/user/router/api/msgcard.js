const express = require('express')
const router = express.Router()
const Msgcard = require('../../model/Msgcard')
const passport = require('passport')
var url = require("url")
router.post('/addmsgcard/:patientID',passport.authenticate('jwt', { session: false }), (req, res) => {
    const card = {}
    if(req.body.name) card.name = req.body.name
    if(req.body.age) card.age = req.body.age
    if(req.body.sex) card.sex = req.body.sex
    if(req.body.phone) card.phone = req.body.phone
    if(req.body.address) card.address = req.body.address
    if(req.body.CHID) card.CHID = req.body.CHID
    card.patientID = req.params.patientID
    new Msgcard(card).save().then( card => {
        return res.json({status:200,msg:'success'})
    }) 
})
router.get('/msgcardlist/:patientID',passport.authenticate('jwt', { session: false }), (req, res) => {
    Msgcard.find({patientID:req.params.patientID})
           .then( cardlist => {
               return res.json({status:200,cardList:cardlist})
           })
})
router.get('/msgcardinfo/:id',passport.authenticate('jwt', { session: false }), (req, res) => {
    Msgcard.findOne({_id:req.params.id}).then( info => {
        return res.json({status:200,cardinfo:info})
    })
})
router.post('/editmsgcard/:id',passport.authenticate('jwt', { session: false }), (req, res) => {
    const card = {}
    if(req.body.name) card.name = req.body.name
    if(req.body.age) card.age = req.body.age
    if(req.body.sex) card.sex = req.body.sex
    if(req.body.address) card.address = req.body.address
    if(req.body.CHID) card.CHID = req.body.CHID
    Msgcard.findOneAndUpdate({_id:req.params.id},{$set:card},{new:true}).then(function(){
        return res.json({status:200,msg:'success'})
    })
})
router.delete('/delete/:id',passport.authenticate('jwt', { session: false }), (req, res) => {
    Msgcard.deleteOne({_id:req.params.id}).then( () => {
        return res.json({status:200,msg:'success'})
    })
})
module.exports = router