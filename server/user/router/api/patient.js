const express = require('express')
const router = express.Router()
const Patient = require('../../model/Patient')
// const JwtStrategy = require('passport-jwt').Strategy,
const passport = require('passport')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const  keys =  require('../../config/keys')
var url = require("url")
router.post('/login', (req, res) => {
    const username = req.body.username
    const password = req.body.password
    Patient.findOne({ username: username })
        .then(patient => {
            if (!patient) {
                return res.status(201).json('用户名不存在')
            }else if(patient && patient.tag === 0){
                return res.json({status:202})
            }
            bcrypt.compare(password, patient.password)
                .then(isMath => {
                    if (isMath) {
                        const rule = { id: patient.id, name: patient.name }
                        jwt.sign(rule, keys.secretOrKey, { expiresIn: 3600 }, (err, token) => {
                            if (err) return err
                            res.json({
                                status:200,
                                success: true,
                                token: 'Bearer ' + token,
                                patient: patient
                            })
                        })
                    } else {
                        return res.json({status:203})
                    }
                })
        })

})
router.post('/register', (req, res) => {
    if(req.body.username&&req.body.password){
    Patient.findOne({ username: req.body.username })
        .then(patient => {
            if (patient) {
                return res.json({msg:'此用户信息已存在',status:201})
            }
            const newPatient = new Patient({
                username: req.body.username,
                password: req.body.password
            })
            bcrypt.genSalt(10, function (err, salt) {
                bcrypt.hash(newPatient.password, salt, (err, hash) => {
                    if (err) throw err;
                    newPatient.password = hash;
                    newPatient.save()
                        .then(patient => res.json({
                            user:patient,
                            status:200
                        }))
                        .catch(err => console.log(err))
                    // Store hash in your password DB.
                })
            })

        })
    }else{
        return
    }
})
router.post('/perfect/:id',passport.authenticate('jwt', { session: false }), (req, res) => {
    const newPersonMsg = {}
    if(req.body.personmessage) newPersonMsg.personmessage = req.body.personmessage
    Patient.findOneAndUpdate({_id:req.params.id},{$set:newPersonMsg},{new:true})
           .then(patient => {
               return res.json(({
                user:patient,
                status:200
            }))
           })
})
router.get('/patientList/:id',passport.authenticate('jwt', { session: false }), (req, res) => {
    Patient.findOne({_id:req.params.id})
           .then( patient => {
               if(!patient){return res.json({status:201,msg:'未查到此人'})}
               res.json({status:200,user:patient})
           })
})
router.get('/patientList',(req, res) => {
        Patient.find({}).then( list => {
            return res.json({status:200,userList:list})
        })
                
})
router.delete('/delete/:id',(req, res) => {
    Patient.deleteOne({_id:req.params.id}).then(()=>{
        return res.json({status:200})
    })
})
router.post('/djuser/:id',(req,res) => {
    Patient.findOneAndUpdate({_id:req.params.id},{$set:{tag:0}},{new:true}).then(()=>{
         return res.json({status:200})
    })
})
module.exports = router

