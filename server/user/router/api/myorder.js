const express = require('express')
const router = express.Router()
const MyOrder = require('../../model/MyOrder')
// const JwtStrategy = require('passport-jwt').Strategy,
const passport = require('passport')
var url = require("url")
router.post('/add/:patientID',passport.authenticate('jwt', { session: false }), (req, res) => {
    const newOrder = {}
    if(req.body.name) newOrder.name = req.body.name
    if(req.body.orderDate) newOrder.visitDate = req.body.visitDate
    if(req.body.orderID) newOrder.orderID = req.body.orderID
    newOrder.patientID = req.params.patientID
    new MyOrder(newOrder).save()
                         .then( myorder => {
                             return res.json({
                                 msg:'success',
                                 status:200
                             })
                         })

})
router.get('/orderlist/:patientID',passport.authenticate('jwt', { session: false }), (req, res) => {
    MyOrder.find({patientID:req.params.patientID})
           .then( myorder => {
               return res.json({
                   status:200,
                   orderList:myorder
               })
           })
})
router.post('/edit/:orderID',(req,res) => {
    const newInfo = {}
    if(req.body.visitDate) newInfo.visitDate = req.body.visitDate
    if(req.body.address) newInfo.address = req.body.address
    MyOrder.findOneAndUpdate({orderID:req.params.orderID},{$set:newInfo},{new:true})
           .then( edit => {
               return res.json({
                   status:200,
                   msg:'success'
               })
           })
})
// router.get('/allorder',(req,res)  => {
//     const queryInfo = url.parse(req.url, true).query
//     const size = parseInt(queryInfo.pagesize)
//     const page = parseInt(queryInfo.pagenum)
//     const query = queryInfo.query
//     const start = (page - 1) * size
//     console.log(req.url)
//     if(query){
//         MyOrder.find().where('name',query).then(orders => {
//             if(!orders){
//                 return res.status(201).json('未查到此人')
//             }
//             return res.json({orderlist:orders,status:200,total:orders.length})
//         })
//     }else{
//         MyOrder.find()
//         .then(count => {
//             console.log(count)
//             const total = count.length
//             MyOrder.find().limit(size).skip(start)
//                 .then(orders => {
//                     if (!orders) {
//                         return
//                     }
//                     return res.json({status:200,orderlist:orders,total:total})
//                 })
//                 .catch(err => res.status(400).json(err))
//         })
//     }
// })
module.exports = router