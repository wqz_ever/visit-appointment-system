const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Order = require('../model/Order')
const MyOrderSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    orderDate:{
        type:Date,
        default:Date.now()
    },
    visitDate:{
        type:Date,
        default:''
    },
    address:{
        type:String,
        default:''
    },
    patientID:{
        type:mongoose.Schema.Types.ObjectId,
        ref:Patient
    },
    orderID:{
        type:mongoose.Schema.Types.ObjectId,
        ref:Order
    }
})
module.exports = MyOrder = mongoose.model('myorder',MyOrderSchema)