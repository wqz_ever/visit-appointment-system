const mongoose = require('mongoose')
const Schema  = mongoose.Schema;
const PatientSchema = new Schema({
    username:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    date:{
        type:Date,
        default:Date.now()
    },
    tag:{
        type:Number,
        default:1
    }
})
module.exports = Patient = mongoose.model('patient',PatientSchema)