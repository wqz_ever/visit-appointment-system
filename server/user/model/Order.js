const mongoose = require('mongoose');
const Patient = require('./Patient');
const Schema  = mongoose.Schema;
const OrderSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    age:{
        type:String,
        required:true
    },
    sex:{
        type:String,
        required:true
    },
    symptom:{
        type:Array,
        required:true
    },
    phone:{
        type:String,
        required:true
    },
    date:{
        type:Date,
        default:Date.now()
    },
    orderDate:{
        type:Date,
        default:''
    },
    tag:{
        type:String,
        default:'1'
    },
    patientID:{
        type:mongoose.Schema.Types.ObjectId,
        ref:Patient
    }
})
module.exports =  Order  = mongoose.model("Order",OrderSchema)