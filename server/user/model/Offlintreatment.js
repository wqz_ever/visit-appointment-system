const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Msgcard = require('../model/Msgcard')
const Doctor = require('../model/Doctor')
const OfflinetreatmentSchema = new Schema({
    orderDate:{
        type:Date,
        default:Date.now()
    },
    offlineDate:{
        type:Date,
        default:''
    },
    msgCardID:{
        type:mongoose.Schema.Types.ObjectId,
        ref:Msgcard
    },
    status:{
        type:Number,
        default:0
    },
    doctorID:{
        type:mongoose.Schema.Types.ObjectId,
        ref:Doctor
    },
    patientID:{
        type:mongoose.Schema.Types.ObjectId,
        ref:Patient
    },
    message:{
        type:String,
        default:''
    }
})
module.exports = Offlinetreatment = mongoose.model('offlinetreatment',OfflinetreatmentSchema)