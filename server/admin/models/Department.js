const mongoose = require('mongoose')
const Schema = mongoose.Schema
const DepartmentSchema = new Schema({
    hospitalname:{
        type:String,
        required:true
    },
    departmentname:{
        type:String,
        required:true
    }
})
module.exports = Department = mongoose.model('department',DepartmentSchema)