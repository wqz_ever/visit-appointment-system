const mongoose = require('mongoose')
const Schema = mongoose.Schema
const HospitalSchema = new Schema({
    hospitalname:{
        type:String,
        required:true
    }
})
module.exports = Hospital = mongoose.model('hospital',HospitalSchema)