const express = require('express')
const router = express.Router()
const Doctor = require('../../models/Doctor')
const passport = require('passport')
var url = require("url")
router.post('/addinfo', passport.authenticate('jwt', { session: false }), (req, res) => {
    const newDoctorInfo = {}
    if (req.body.jobNumber) newDoctorInfo.jobNumber = req.body.jobNumber
    if (req.body.name) newDoctorInfo.name = req.body.name
    if (req.body.age) newDoctorInfo.age = req.body.age
    if (req.body.sex) newDoctorInfo.sex = req.body.sex
    if (req.body.hospital) newDoctorInfo.hospital = req.body.hospital
    if (req.body.department) newDoctorInfo.department = req.body.department
    if (req.body.adress) newDoctorInfo.adress = req.body.adress
    if (req.body.position) newDoctorInfo.position = req.body.position
    if (req.body.phone) newDoctorInfo.phone = req.body.phone
    if (req.body.email) newDoctorInfo.email = req.body.email
    if (req.body.personalProfile) newDoctorInfo.personalProfile = req.body.personalProfile
    new Doctor(newDoctorInfo).save().then(doctor => {
        res.status(200).json(doctor)
    })
        .catch(err => {
            res.status(400).json(err)
        })
})
router.get('/doctorInfo/:jobNumber', (req, res) => {
    Doctor.findOne({ jobNumber: req.params.jobNumber })
        .then(doctor => {
            if (doctor) {
                return res.status(200).json(doctor)
            }
            res.status(201).json('未查到此人')
        })
        .catch(err => {
            res.status(400).json(err)
        })
})
router.get('/doctormsg/:id', (req, res) => {
    Doctor.findOne({ _id: req.params.id })
        .then(doctor => {
            if (doctor) {
                return res.json({status:200,doctor:doctor})
            }else{
                return res.json({status:201,msg:'error'})
            }
        })
        .catch(err => {
            res.status(400).json(err)
        })
})
router.get('/doctorList', passport.authenticate('jwt', { session: false }), (req, res) => {
    const queryInfo = url.parse(req.url, true).query
    const size = parseInt(queryInfo.pagesize)
    const page = parseInt(queryInfo.pagenum)
    const query = queryInfo.query
    const start = (page - 1) * size
    if (query) {
        Doctor.where('name', query).then(doctor => {
            if (!doctor) {
                return res.status(201).json('未查到此人')
            }
            const newDoctorList = { doctorList: doctor, total: doctor.length }
            res.status(200).json(newDoctorList)
        })
    } else {
        Doctor.find({})
            .then(count => {
                const total = count.length
                Doctor.find({}).limit(size).skip(start)
                    .then(doctor => {
                        if (!doctor) {
                            return res.status(201).json("获取列表数据失败")
                        }
                        const newDoctorList = { doctorList: doctor, total: total }
                        res.status(200).json(newDoctorList)
                    })
                    .catch(err => res.status(400).json(err))
            })
    }
})
router.delete('/delete/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    Doctor.findByIdAndDelete({ _id: req.params.id })
        .then(doctor => {
                return res.status(200).json('删除成功')
        })
        .catch(err => {
            res.status(201).json(err)
        })
})
router.post('/edit/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    let newDoctorInfo = {}
    if (req.body.name) newDoctorInfo.name = req.body.name
    if (req.body.age) newDoctorInfo.age = req.body.age
    if (req.body.sex) newDoctorInfo.sex = req.body.sex
    if (req.body.hospital) newDoctorInfo.hospital = req.body.hospital
    if (req.body.department) newDoctorInfo.department = req.body.department
    if (req.body.adress) newDoctorInfo.adress = req.body.adress
    if (req.body.position) newDoctorInfo.position = req.body.position
    if (req.body.phone) newDoctorInfo.phone = req.body.phone
    if (req.body.email) newDoctorInfo.email = req.body.email
    if (req.body.personalProfile) newDoctorInfo.personalProfile = req.body.personalProfile
    Doctor.findOneAndUpdate({ _id: req.params.id }, { $set: newDoctorInfo }, { new: true })
        .then(doctor => {
            return res.json(
                {
                    status: 200,
                    msg:'success'
                }

            )
        })
})
router.get('/list',(req, res) => {
    Doctor.find({}).then(list => {
          list.forEach( item => {
              item.jobNumber = ''
          })
          return res.json({status:200,doctorList:list})
    })
         
})
module.exports = router